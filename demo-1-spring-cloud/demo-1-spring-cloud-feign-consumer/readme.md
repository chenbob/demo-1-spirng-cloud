开启feign.hystrix 的注意点：
1、application.yml

    feign:
      hystrix:
    	   enabled: true

2、实现访问接口要在bean中,@component必须
    
    @Component
    public class EmployeeServiceCallFall implements EmployeeService {}