package com.demo.spring.cloud.feign.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 
 * @author Administrator
 *
 */
@SpringBootApplication
//@EnableDiscoveryClient
//@EnableDiscoveryClient基于spring-cloud-commons, @EnableEurekaClient基于spring-cloud-netflix。
//其实用更简单的话来说，就是如果选用的注册中心是eureka，那么就推荐@EnableEurekaClient，如果是其他的注册中心，那么推荐使用@EnableDiscoveryClient。
@EnableEurekaClient
@EnableFeignClients//必须加上这个哈
@EnableHystrix
public class FeignConsumerApplicationStart {
	public static void main(String[] args) {
		SpringApplication.run(FeignConsumerApplicationStart.class, args);
	}

}
