package com.demo.spring.cloud.feign.consumer.service;

import org.springframework.stereotype.Component;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;

@Component
public class EmployeeServiceCallFall implements EmployeeService {

	@Override
	public Employee addEmployee(Employee employee) {
		return null;
	}

	@Override
	public Employee findEmployee(String employeeId) throws Exception{
		Employee employee = new Employee();
		employee.setId("default");
		return employee;
	}

	@Override
	public PageResult<Employee> findEmployeePage(Integer page, Integer size) throws Exception {
		return null;
	}

}
