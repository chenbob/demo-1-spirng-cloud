package com.demo.spring.cloud.feign.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;

@FeignClient(name="ribbon-service-employee-provider",fallback = EmployeeServiceCallFall.class)
//@FeignClient(name="ribbon-service-employee-provider",fallbackFactory = HystrixClientFallbackFactory.class)
public interface EmployeeService {
	
	@RequestMapping(method = RequestMethod.POST, value = "/employee/addEmployee")
	public Employee addEmployee(@RequestBody Employee employee);
	
	@RequestMapping(method = RequestMethod.GET, value = "/employee/findEmployee/{employeeId}")
	public Employee findEmployee(@PathVariable("employeeId") String employeeId) throws Exception;
	
	@RequestMapping(method = RequestMethod.GET, value = "/employee/findEmployee/{page}/{size}")
	public PageResult<Employee> findEmployeePage(@PathVariable("page") Integer page,@PathVariable("size") Integer size) throws Exception;
	
	
}
