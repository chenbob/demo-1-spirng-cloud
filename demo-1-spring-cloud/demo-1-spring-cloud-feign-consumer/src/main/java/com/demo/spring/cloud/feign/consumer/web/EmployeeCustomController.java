package com.demo.spring.cloud.feign.consumer.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;
import com.demo.spring.cloud.feign.consumer.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/feign")
public class EmployeeCustomController {
	
	@Autowired
	private EmployeeService employeeService;
	 	 
	@PostMapping("/addEmployee")
	public Employee addEmployee(@RequestBody Employee employee) {
		log.info("{}",employee.getId());
		return employeeService.addEmployee(employee);
	}

    @PostMapping("/findEmployee/{employeeId}")
    public Employee findEmployee(@PathVariable("employeeId") String employeeId) throws Exception {
    	return employeeService.findEmployee(employeeId);
    }
    
    @PostMapping("/findEmployee/{page}/{size}")
    public PageResult<Employee> findEmployeePage(@PathVariable("page") Integer page,@PathVariable("size") Integer size) throws Exception {
    	return employeeService.findEmployeePage(page, size);
    }
}
