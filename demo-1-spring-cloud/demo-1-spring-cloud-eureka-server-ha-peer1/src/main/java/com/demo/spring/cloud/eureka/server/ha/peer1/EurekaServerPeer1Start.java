package com.demo.spring.cloud.eureka.server.ha.peer1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Hello world!
 *
 */

@SpringBootApplication
@EnableEurekaServer
public class EurekaServerPeer1Start {
	public static void main(String[] args) {
		SpringApplication.run(EurekaServerPeer1Start.class, args);
	}
}
