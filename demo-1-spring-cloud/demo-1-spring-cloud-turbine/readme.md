开启turbine注意点：
1、监控的各服务需要开启hystrix,`@EnableHystrix`

2、配置 /hystrix.stream 可以访问

    management:
      endpoints:
    		web:
				exposure:
					include: '*'

3、feign配置可能还需要添加：

    	feign:
			hystrix:
    			enabled: true
				
4、监控服务只监控有熔断器方法实现的方法。
	对Ribbon 是有注解@HystrixCommand，
	对feign是配置有@FeignClient(name="ribbon-service-employee-provider",fallback = **EmployeeServiceCallFall**.class) 或者
				   @FeignClient(name="ribbon-service-employee-provider",**fallbackFactory** = HystrixClientFallbackFactory.class)