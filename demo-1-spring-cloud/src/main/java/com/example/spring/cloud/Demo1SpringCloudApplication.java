package com.example.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo1SpringCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(Demo1SpringCloudApplication.class, args);
	}
}
