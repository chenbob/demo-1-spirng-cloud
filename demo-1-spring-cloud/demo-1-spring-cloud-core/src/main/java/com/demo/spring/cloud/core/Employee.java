package com.demo.spring.cloud.core;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Document(indexName = "megacorp",type = "employee", shards = 1,replicas = 0, refreshInterval = "-1")
public class Employee implements Serializable{
	
	private static final long serialVersionUID = -755134385317431304L;
	
	@Id
	private String id;
	@Field(type = FieldType.Text)
	private String firstName;
	@Field(type = FieldType.Text)
	private String lastName;
	@Field(type = FieldType.Integer)
	private Integer age = 0;
	@Field(type = FieldType.Text)
	private String about;
	
}
