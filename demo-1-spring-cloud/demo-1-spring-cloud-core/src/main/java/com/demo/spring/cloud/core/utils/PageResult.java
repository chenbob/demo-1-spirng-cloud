package com.demo.spring.cloud.core.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class PageResult<T> implements Serializable{

	private static final long serialVersionUID = -6802934658740817885L;
	
	private List<T> content = new ArrayList<T>();
	
	private int pageNum;
	
	private int pageSize;
	
	private int pageElements;
	
	private long totalElements;
	
	private int totalPages;
	
}
