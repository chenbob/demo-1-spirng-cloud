package com.demo.spring.cloud.zuul.configure;

import org.springframework.cloud.netflix.zuul.filters.discovery.PatternServiceRouteMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 添加带版本号的正则解析
 * @author Administrator
 *
 */
@Configuration
public class ApplicationConfiguration {
	
	@Bean
	public PatternServiceRouteMapper serviceRouteMapper() {
	    return new PatternServiceRouteMapper(
	        "(?<name>^.+)-(?<version>v.+$)",
	        "${version}/${name}");
	}
	//servicename-v1   ---> v1/servicename
}
