/****************************************************************************************
Zuul 路由后面的微服务挂了后，Zuul 提供了一种回退机制来应对熔断处理：
关闭ribbon-service-employee-provider服务后

http://localhost:8001/api5/employee/findEmployee/5  ->fallback

http://localhost:8001/ribbon-service-employee-provider/employee/findEmployee/5  返回的404页面

 ****************************************************************************************/