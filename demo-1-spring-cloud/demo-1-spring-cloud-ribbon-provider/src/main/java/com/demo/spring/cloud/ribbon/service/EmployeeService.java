package com.demo.spring.cloud.ribbon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.demo.spring.cloud.core.Employee;

import com.demo.spring.cloud.ribbon.repository.EmployeeRepository;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public Employee addEmployee(Employee e) {
		return this.employeeRepository.save(e);
	}
	
	public Employee findEmployee(String id) {
		return this.employeeRepository.findById(id).get();
	}
	
	public Page<Employee> findEmployee(int page,int size) {
		Pageable pageable = PageRequest.of(page, size, Sort.by(new Sort.Order(Direction.ASC,"id")));
		Page<Employee> employees = this.employeeRepository.findAll(pageable);
		return employees;
	}
	
}
