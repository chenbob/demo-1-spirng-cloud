package com.demo.spring.cloud.ribbon.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingUrlCheck {
	
	@RequestMapping("/")
	public String ping() {
		return "pong";
	}

}
