package com.demo.spring.cloud.ribbon.customer.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.spring.cloud.core.Employee;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class EmployeeService {

	@Autowired
	private RestTemplate restTemplate;

	private static final String PATH = "http://ribbon-service-employee-provider/";

	@HystrixCommand(fallbackMethod = "addEmployeeFall")
	public Employee callAddEmployee(Employee employee) {
		if(employee == null || employee.getId() == null) {
			throw new RuntimeException("no empoyee put");
		}
		String url = PATH + "employee/addEmployee";
		// 设置请求的header
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json;charset=utf-8");
		headers.set("Accept", "application/json");
		HttpEntity<Employee> request = new HttpEntity<Employee>(employee, headers);
		return restTemplate.postForObject(url, request, Employee.class);
	}
	
	 public Employee addEmployeeFall(Employee employee) {//和主方法申明一致
	    	Employee employee0 = new Employee();
	    	employee0.setId(UUID.randomUUID().toString());
			return employee0;
	    }

}
