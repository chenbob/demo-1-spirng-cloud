package com.demo.spring.cloud.ribbon.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.demo.spring.cloud.ribbon.conf.RibbonCustomConfiguration;

/**
 * 
 * @author Administrator
 *
 */
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "ribbon-service-employee-provider",configuration = RibbonCustomConfiguration.class)
public class RibbonConsumerCustomApplicationStart {
	public static void main(String[] args) {
		SpringApplication.run(RibbonConsumerCustomApplicationStart.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
