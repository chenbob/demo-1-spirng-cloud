package com.demo.spring.cloud.ribbon.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;
import com.netflix.loadbalancer.RandomRule;

/**
 * 
 * @author Bob
 * 自定义的ribbon配置类
 *
 */
@Configuration
public class RibbonCustomConfiguration {
	
	@Autowired
	IClientConfig config;
	
	@Bean
    public IPing ribbonPing(IClientConfig config) {//一定要再服务提供方提供对根应用的访问 例如：demo-1-spring-cloud-ribbon-provider设置的 PingCheck
        return new PingUrl();
    }
	
	@Bean
	public IRule ribbonRule(IClientConfig config) {
		return new RandomRule();
	}

}
