# 自定义RIBBON的配置  #

1、将自定义的配置放到@ComponentScan的扫描包外，否则配置全局有效

2、可以自定义@ComponentScan中的filter方式，排除加载

3、@RibbonClient(name="xxx" 这个name不是随便写的，而是注册到Eureka发现组件上的微服务服务端

4、@RibbonClient 可以有两种写法：
1、直接写在启动类型，T为自定义的ribbon配置类，在@ComponentScan扫描之外

    @SpringBootApplication
    @RibbonClient(name="xxx",class=T.class)
    public class XXXX{
    
    
    }

2、定义在一个扫描类上,T1为自定义的ribbon配置类，在@ComponentScan扫描之外，T2在@ComponentScan之内

    @Configuration
    @RibbonClient(name="xxx",class=T1.class)
    public class T2{
    
    }

5、@RibbonClients可以配置多个


6、再自定的配置RibbonCustomConfiguration中添加了如下代码后遇到的异常，无可用的实例的错误，跟踪代码后，这样的配置服务调用中会检查PingUrl.isAlive()，检查服务提供方是否在线。如果不做对 / 的返回值的配置，会返回false
    
    @Bean
    public IPing ribbonPing(IClientConfig config) {
    	return new PingUrl();
    }