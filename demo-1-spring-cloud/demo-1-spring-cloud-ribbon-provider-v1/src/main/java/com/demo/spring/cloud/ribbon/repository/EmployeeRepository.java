package com.demo.spring.cloud.ribbon.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

import com.demo.spring.cloud.core.Employee;


@Component  
public interface EmployeeRepository extends ElasticsearchRepository<Employee,String>{  
      
    Employee queryEmployeeById(String id);
    
    
  
}  