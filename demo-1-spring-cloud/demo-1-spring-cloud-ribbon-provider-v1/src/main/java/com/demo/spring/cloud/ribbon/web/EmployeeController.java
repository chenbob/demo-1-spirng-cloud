package com.demo.spring.cloud.ribbon.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;
import com.demo.spring.cloud.ribbon.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @GetMapping("/findEmployee/{employeeId}")
    public Employee findEmployee(@PathVariable("employeeId") String employeeId) throws Exception {
        return employeeService.findEmployee(employeeId);
    }
    
    @GetMapping("/findEmployee/{page}/{size}")
    public PageResult<Employee> findEmployeePage(@PathVariable("page") Integer page,@PathVariable("size") Integer size) throws Exception {
    	//返回Page<T> 会因为Page无法实例化出现错误，PageImpl也一样。需要有默认的构造函数
    	
    	Page<Employee> employees = employeeService.findEmployee(page,size);
    	
    	PageResult<Employee> pageResult = new PageResult<Employee>();
    	pageResult.setContent(employees.getContent());
    	pageResult.setPageNum(page);
    	pageResult.setPageSize(size);
    	pageResult.setPageElements(employees.getNumberOfElements());
    	pageResult.setTotalElements(employees.getTotalElements());
    	pageResult.setTotalPages(employees.getTotalPages());
        return pageResult;
    }
}
