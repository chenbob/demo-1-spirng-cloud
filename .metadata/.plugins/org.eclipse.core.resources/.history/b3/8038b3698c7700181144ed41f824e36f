package com.demo.spring.cloud.feign.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;

import feign.hystrix.FallbackFactory;

//@FeignClient(name="ribbon-service-employee-provider",fallback = EmployeeServiceCallFall.class ,fallbackFactory = EmployeeService.HystrixClientFallbackFactory.class)
@FeignClient(name="ribbon-service-employee-provider",fallback = EmployeeServiceCallFall.class ,fallbackFactory = EmployeeService.HystrixClientFallbackFactory.class)
public interface EmployeeService {
	
	@RequestMapping(method = RequestMethod.POST, value = "/employee/addEmployee")
	public Employee addEmployee(@RequestBody Employee employee);
	
	@RequestMapping(method = RequestMethod.GET, value = "/employee/findEmployee/{employeeId}")
	public Employee findEmployee(@PathVariable("employeeId") String employeeId) throws Exception;
	
	@RequestMapping(method = RequestMethod.GET, value = "/employee/findEmployee/{page}/{size}")
	public PageResult<Employee> findEmployeePage(@PathVariable("page") Integer page,@PathVariable("size") Integer size) throws Exception;

	//第二种实现方式使用 FallbackFactory
	@Component
	static class HystrixClientFallbackFactory implements FallbackFactory<EmployeeService> {
		@Override
		public EmployeeService create(Throwable cause) {
			return new EmployeeService() {

				@Override
				public Employee addEmployee(Employee employee) {
					return null;
				}

				@Override
				public Employee findEmployee(String employeeId) throws Exception {
					return null;
				}

				@Override
				public PageResult<Employee> findEmployeePage(Integer page, Integer size) throws Exception {
					return null;
				}
				
			};
		}
	}
}
