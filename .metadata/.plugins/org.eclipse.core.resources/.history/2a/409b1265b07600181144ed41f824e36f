package com.demo.spring.cloud.ribbon.customer.web;

import java.util.UUID;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.spring.cloud.core.Employee;
import com.demo.spring.cloud.core.utils.PageResult;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class EmployeeCustomController {
	
	 private EmployeeCustomControllerData data = new EmployeeCustomControllerData();

	private static final String PATH = "http://ribbon-service-employee-provider/";
	 
	@PostMapping("/addEmployee")
	@HystrixCommand(fallbackMethod="addEmployeeFall")
	public Employee addEmployee(@RequestBody Employee employee) {
		String url = PATH + "employee/addEmployee";
		// 设置请求的header
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json;charset=utf-8");
		headers.set("Accept", "application/json");
		HttpEntity<Employee> request = new HttpEntity<Employee>(employee, headers);
		return data.restTemplate.postForObject(url, request, Employee.class);
	}

    @PostMapping("/findEmployee/{employeeId}")
    public Employee findEmployee(@PathVariable("employeeId") String employeeId) throws Exception {
    	//String url = PATH + String.format("employee/findEmployee/%s", employeeId);
    	String url = PATH + "employee/findEmployee/{employeeId}";
    	return data.restTemplate.getForObject(url, Employee.class ,employeeId);
    }
    
    @PostMapping("/findEmployee/{page}/{size}")
    public PageResult<Employee> findEmployeePage(@PathVariable("page") Integer page,@PathVariable("size") Integer size) throws Exception {
    	String url = PATH + "employee/findEmployee/{page}/{size}";
    	//返回复杂类型的处理方式
    	ParameterizedTypeReference<PageResult<Employee>> responseType = new ParameterizedTypeReference<PageResult<Employee>>() {};
    	ResponseEntity<PageResult<Employee>> responseEntity = data.restTemplate.exchange(url, HttpMethod.GET, null, responseType, page , size);
    	return responseEntity.getBody();
    }
    
    public Employee addEmployeeFall() {
    	Employee employee = new Employee();
    	employee.setId(UUID.randomUUID().toString());
		return employee;
    }
    
}
